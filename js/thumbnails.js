;(function () {
  'use strict'

  if ('IntersectionObserver' in window) {
    /* Lazy load images */
    var links = document.querySelectorAll('.lazyload')
    var observer = new IntersectionObserver(
      function (entries) {
        entries.forEach(function (entry) {
          var isIntersecting = typeof entry.isIntersecting === 'boolean' ? entry.isIntersecting : entry.intersectionRatio > 0
          if (isIntersecting) {
            observer.unobserve(entry.target)
            var image = entry.target.getAttribute('data-img')
            if (image) {
              entry.target.innerHTML = image
            }
          }
        })
      },
      {
        rootMargin: '200px'
      }
    )
    links.forEach(function (link) {
      observer.observe(link)
    })
  } else {
    /* Fallback: Load all images */
    var images = document.getElementsByTagName('img')
    var count = images.length
    for (var i = 0; count > i; i++) {
      var image = images[i].getAttribute('data-img')
      if (image) {
        images[i].innerHTML = image
      }
    }
  }
})()
