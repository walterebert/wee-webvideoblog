;(function () {
  'use strict';

  function getRandomInt(max) {
    return -4 + Math.floor(Math.random() * Math.floor(max))
  }

  if ('matchMedia' in window && window.matchMedia('(min-width: 1024px)')) {
    var positionStyles = '@supports(mask-size: contain) {' +
      '.articles article:first-child {left: ' + getRandomInt(31) + 'px; top: ' + getRandomInt(35) + 'px;}';
    for (var i = 2 ; i <= 7; i++) {
      positionStyles += '.articles article:nth-child(' + i + 'n) {left: ' + getRandomInt(31) + 'px; top: ' + getRandomInt(35) + 'px;}'
    }
    positionStyles += '}'

    var style = document.createElement('style')
    style.innerHTML = positionStyles
    document.getElementById('tiles-js').parentNode.appendChild(style)
  }
})()
