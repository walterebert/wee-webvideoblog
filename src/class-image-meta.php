<?php
/**
 * Add meta data to images
 *
 * @package WordPress
 * @subpackage WebVideoBlog
 */

namespace wee\WebVideoBlog;

use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;

/**
 * Image meta data class
 */
class Image_Meta {
	const DOMINANT_COLOR = 'webvideoblog_dominant_color';

	/**
	 * Delete dominant color
	 *
	 * @param integer $attachment_id Image ID.
	 */
	public static function delete_dominant_color( $attachment_id ) {
		if ( \is_numeric( $attachment_id ) ) {
			\delete_post_meta( $attachment_id, self::DOMINANT_COLOR );
		}
	}

	/**
	 * Extract dominant color from image
	 *
	 * @param integer $attachment_id Image ID.
	 */
	public static function extract_dominant_color( $attachment_id ) {
		if ( ! \wp_attachment_is_image( $attachment_id ) ) {
			return false;
		}

		$file = \get_attached_file( $attachment_id );
		if ( $file ) {
			$palette = Palette::fromFilename( $file );
			$extractor = new ColorExtractor( $palette );
			$colors = $extractor->extract( 1 );
			if ( isset( $colors[0] ) ) {
				return Color::fromIntToHex( $colors[0] );
			}
		}

		return false;
	}

	/**
	 * Save dominant color
	 *
	 * @param integer $attachment_id Image ID.
	 */
	public static function save_dominant_color( $attachment_id ) {
		$color = self::extract_dominant_color( $attachment_id );
		if ( $color ) {
			\update_post_meta( $attachment_id, self::DOMINANT_COLOR, $color );
		}
	}
}
