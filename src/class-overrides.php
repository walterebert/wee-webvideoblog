<?php
/**
 * Override plugin functionality
 *
 * @package WordPress
 * @subpackage WebVideoBlog
 */

namespace wee\WebVideoBlog;

/**
 * Override class
 */
class Overrides {
	/**
	 * Dequeue scripts and styles
	 */
	public static function dequeue() {
		\wp_dequeue_script( 'contact-form-7' );
		\wp_dequeue_style( 'contact-form-7' );
	}

	/**
	 * Load scripts and styles for selected short codes
	 *
	 * @param string $content The content.
	 * @return string
	 */
	public static function shortcodes( $content ) {
		// Only load Contact Form 7 CSS + JS if a form is present.
		if ( \has_shortcode( $content, 'contact-form-7' ) && \function_exists( 'wpcf7_plugin_url' ) ) {
			// phpcs:disable WordPress.WP.EnqueuedResources.NonEnqueuedStylesheet
			$css = '<link href="' . \esc_url( \wpcf7_plugin_url( 'includes/css/styles.css' ) . '?ver=' . WPCF7_VERSION ) . '" rel="stylesheet" type="text/css">';
			if ( \wpcf7_is_rtl() ) {
				$css .= '<link href="' . \esc_url( \wpcf7_plugin_url( 'includes/css/styles-rtl.css' ) . '?ver=' . WPCF7_VERSION ) . '" rel="stylesheet" type="text/css">';
			}
			$css .= '<link href="' . \esc_url( \get_theme_file_uri() . '/css/forms.min.css?ver=' . \wp_get_theme()->Version ) . '" rel="stylesheet" type="text/css">';
			$content = "\n$css\n$content";
			// phpcs:enable

			\wpcf7_enqueue_scripts();
		}

		return $content;
	}
}
