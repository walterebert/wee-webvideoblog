<?php
/**
 * Theme methods
 *
 * @package WordPress
 * @subpackage WebVideoBlog
 */

namespace wee\WebVideoBlog;

/**
 * Main theme class
 */
class Theme {
	const SWIPER_VERSION = '4.5.1';

	/**
	 * Set image quality for JPEG
	 */
	public static function jpeg_quality() {
		if ( \defined( 'WEBVIDEOBLOG_JPEG_QUALITY' ) ) {
			return \absint( WEBVIDEOBLOG_JPEG_QUALITY );
		}
		return 85;
	}

	/**
	 * Add custom settings
	 *
	 * @param object $context Timber context.
	 */
	public static function settings( $context ) {
		$context['settings'] = array(
			'prefix'     => 'webvideoblog',
			'textdomain' => 'webvideoblog',
		);

		$context['copyright_holder'] = array(
			'url' => \get_bloginfo( 'url' ),
			'name' => \get_bloginfo( 'name' ),
		);

		if ( getenv( 'COPYRIGHT_HOLDER_URL' ) ) {
			$context['copyright_holder']['url'] = getenv( 'COPYRIGHT_HOLDER_URL' );
		}

		if ( getenv( 'COPYRIGHT_HOLDER_NAME' ) ) {
			$context['copyright_holder']['name'] = getenv( 'COPYRIGHT_HOLDER_NAME' );
		}

		return $context;
	}

	/**
	 * Setup the theme
	 */
	public static function setup() {
		\load_theme_textdomain( 'webvideoblog', \get_template_directory() . DIRECTORY_SEPARATOR . 'languages' );

		\add_theme_support( 'automatic-feed-links' );

		\add_theme_support(
			'custom-header',
			array(
				'flex-height'        => true,
				'flex-width'         => true,
				'random-default'     => true,
				'header-text'        => true,
				'default-text-color' => 'fff',
			)
		);

		\add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		\add_theme_support( 'title-tag' );

		\add_theme_support( 'post-thumbnails' );
	}

	/**
	 * Load JavaScript for tiles
	 */
	public static function tiles_js() {
		$file = __DIR__ . '/../js/tiles.min.js';
		if ( WP_DEBUG ) {
			__DIR__ . '/../js/tiles.js';
		}
		return '<script id="tiles-js">' . \file_get_contents( $file ) . '</script>';
	}

	/**
	 * Register WordPress sidebars
	 */
	public static function sidebars() {
		\register_sidebar(
			array(
				'name'          => 'Footer 1',
				'id'            => 'site-footer-1',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4>',
				'after_title'   => '</h4>',
			)
		);

		\register_sidebar(
			array(
				'name'          => 'Footer 2',
				'id'            => 'site-footer-2',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4>',
				'after_title'   => '</h4>',
			)
		);
	}

	/**
	 * Start Timber
	 */
	public static function timber() {
		new \Timber\Timber();
	}

	/**
	 * Change default scripts
	 *
	 * @param object $scripts Registered scripts.
	 */
	public static function wp_default_scripts( $scripts ) {
		$scripts->remove( 'jquery-migrate' );
		$scripts->add( 'jquery-migrate', null );
	}

	/**
	 * Add JS and CSS scripts
	 */
	public static function wp_enqueue_scripts() {
		/* Force Javascript to load in footer */
		\remove_action( 'wp_head', 'wp_print_scripts' );
		\remove_action( 'wp_head', 'wp_print_head_scripts', 9 );
		\remove_action( 'wp_head', 'wp_enqueue_scripts', 1 );

		// Adds JavaScript to pages with the comment form to support sites with
		// threaded comments (when in use).
		if ( \is_singular() && \comments_open() && \get_option( 'thread_comments' ) ) {
			\wp_enqueue_script( 'comment-reply' );
		}

		/* Header slider */
		$theme_mod     = \get_theme_mod( 'header_image', '' );
		$slider_images = Content::header_images( $theme_mod );
		if ( \count( $slider_images ) > 1 ) {
			/* Slider */
			\wp_enqueue_style( 'swiper', \get_theme_file_uri() . '/css/vendor/swiper.min.css', array(), self::SWIPER_VERSION );
			\wp_enqueue_script( 'swiper', \get_theme_file_uri() . '/js/vendor/swiper.min.js', array(), self::SWIPER_VERSION, true );
			$slider_options = '{"preloadImages": false, "lazy": true, "autoplay": true, "loop": true, "effect": "fade", "duration": 600}';
			if ( \defined( 'WEBVIDEOBLOG_SLIDER_OPTIONS' ) ) {
				$custom_options = \json_decode( (string) WEBVIDEOBLOG_SLIDER_OPTIONS );
				if ( $custom_options ) {
					$slider_options = \json_encode( $custom_options );
				}
			}
			\wp_add_inline_script(
				'swiper',
				'new Swiper(".site-header__image", ' . (string) $slider_options . ')'
			);
		}

		/* Theme styles */
		\wp_enqueue_style( 'webvideoblog', \get_theme_file_uri() . '/css/styles.min.css', array(), \wp_get_theme()->Version );
		if ( \is_front_page() || \is_home() || \is_archive() || \is_search() ) {
			\wp_enqueue_style( 'webvideoblog-archive', \get_theme_file_uri() . '/css/archive.min.css', array(), \wp_get_theme()->Version );
		}
		if ( \is_search() ) {
			\wp_enqueue_style( 'webvideoblog-forms', \get_theme_file_uri() . '/css/forms.min.css', array(), \wp_get_theme()->Version );
		}
	}

	/**
	 * Output for HTML head
	 */
	public static function wp_head() {
		$header_text_color = \get_header_textcolor();

		if ( 'blank' === $header_text_color ) {
			$header_text_color = 'fff';
		}

		echo '<style>.site-header {color: #' . \esc_attr( $header_text_color ) . "}</style>\n";
	}
}
