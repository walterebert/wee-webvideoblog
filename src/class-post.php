<?php
/**
 * Custom Timber post
 *
 * @package WordPress
 * @subpackage WebVideoBlog
 */

namespace wee\WebVideoBlog;

/**
 * Customise posts
 */
class Post extends \TimberPost {
	/**
	 * Add functionality of post output
	 */
	public static function get_post() {
		$post = parent::get_post();

		if ( isset( $post->thumbnail->ID ) ) {
			$post->respimg_srcset = \wp_get_attachment_image_srcset( $post->thumbnail->ID, 'webvideoblog-max' );
			$post->respimg_sizes = \wp_get_attachment_image_sizes( $post->thumbnail->ID, 'webvideoblog-max' );
		}

		return $post;
	}
}
