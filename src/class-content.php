<?php
/**
 * Content methods
 *
 * @package WordPress
 * @subpackage WebVideoBlog
 */

namespace wee\WebVideoBlog;

/**
 * Main theme class
 */
class Content {
	/**
	 * Custom header
	 *
	 * @param object $context Timber context.
	 */
	public static function custom_header( $context ) {
		$theme_mod = \get_theme_mod( 'header_image', '' );

		$context['slider_images'] = self::header_images( $theme_mod );

		return $context;
	}

	/**
	 * Footer Widgets
	 *
	 * @param object $context Timber context.
	 */
	public static function footer_widgets( $context ) {
		$context['widget_footer_1'] = \Timber::get_widgets( 'footer-1' );
		$context['widget_footer_2'] = \Timber::get_widgets( 'footer-2' );

		return $context;
	}

	/**
	 * Header images
	 *
	 * @param string $theme_mod WordPress theme mod.
	 * @return array $slider_images Header images
	 */
	public static function header_images( $theme_mod ) {
		global $post;

		$slider_images = array();

		$thumbnail_id = false;
		if ( \is_singular() && isset( $post->id ) ) {
			$thumbnail_id = \get_post_thumbnail_id( $post->id );
		}

		if ( $thumbnail_id ) {
			/* Post Feature Image */
			$thumbnail_data = \wp_get_attachment_image_src( $thumbnail_id, 'webvideoblog-max' );
			if ( $thumbnail_data ) {
				$slider_images = array(
					0 => array(
						'attachment_id' => $thumbnail_id,
						'url'           => $thumbnail_data[0],
						'width'         => $thumbnail_data[1],
						'height'        => $thumbnail_data[2],
						'alt_text'      => \get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true ),
						'dominant_color' => \get_post_meta( $thumbnail_id, 'webvideoblog_dominant_color', true ),
					),
				);
			}
		} elseif ( 'random-uploaded-image' === $theme_mod && \is_front_page() ) {
			/* Random Header Images */
			$slider_images = \get_uploaded_header_images();
		} elseif ( \is_front_page() ) {
			/* Theme Header Image */
			$header_image = (array) \get_theme_mod( 'header_image_data' );
			if ( ! empty( $header_image['attachment_id'] ) ) {
				$header_image_data = \wp_get_attachment_image_src( $header_image['attachment_id'], 'webvideoblog-max' );
				$slider_images     = array(
					0 => array(
						'attachment_id' => $header_image['attachment_id'],
						'url'           => $header_image_data[0],
						'width'         => $header_image_data[1],
						'height'        => $header_image_data[1],
						'alt_text'      => \get_post_meta( $header_image['attachment_id'], '_wp_attachment_image_alt', true ),
						'dominant_color' => \get_post_meta( $header_image['attachment_id'], 'webvideoblog_dominant_color', true ),
					),
				);
			}
		}

		return $slider_images;
	}

	/**
	 * Add menus
	 *
	 * @param object $context Timber context.
	 */
	public static function twig_menus( $context ) {
		$context['navigation'] = new \TimberMenu( 'navigation' );

		return $context;
	}
}
