# webvideo.blog

A WordPress theme using the
[Twig Template Engine](https://twig.symfony.com/),
the [Timber plugin](https://www.upstatement.com/timber/)
and [Sass](http://sass-lang.com/).

To use this theme you have to install all dependencies first.

For PHP run:
```
composer install -o --no-dev
```

For JavaScript and CSS run:
```
npm install
npm run build
```

This theme was created by [Walter Ebert](https://walterebert.com).

## Licenses

[Photo of Apple Macintosh Plus](https://unsplash.com/photos/aiqKc07b5PA) by [Federica Galli](https://unsplash.com/@fedechanw) -  Licensed under the [Unsplash License](https://unsplash.com/license).

[Vollkorn font](http://vollkorn-typeface.com/) by [Friedrich Althausen](http://friedrichalthausen.de/) - Licensed under the [SIL Open Font License](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).

All other resources and theme elements are licensed under the [GNU GPL](https://www.gnu.org/licenses/gpl-2.0.html), version 2 or later.
