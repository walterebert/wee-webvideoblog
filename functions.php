<?php
/**
 * Bootstrap the theme
 *
 * @package WordPress
 * @subpackage WebVideoBlog
 */

namespace wee\WebVideoBlog;

/* Check if theme is called correctly. */
if ( ! \defined( 'ABSPATH' ) ) {
	\header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

/* Composer autoloader */
require __DIR__ . '/vendor/autoload.php';

/* Actions */
\add_action( 'add_attachment', __NAMESPACE__ . '\\ImageMeta::save_dominant_color' );
\add_action( 'after_setup_theme', __NAMESPACE__ . '\\Theme::timber' );
\add_action( 'after_setup_theme', __NAMESPACE__ . '\\Theme::setup' );
\add_action( 'after_switch_theme', __NAMESPACE__ . '\\Theme::set_image_options' );
\add_action( 'delete_attachment', __NAMESPACE__ . '\\ImageMeta::delete_dominant_color' );
\add_action( 'widgets_init', __NAMESPACE__ . '\\Theme::sidebars' );
\add_action( 'wp_default_scripts', __NAMESPACE__ . '\\Theme::wp_default_scripts', PHP_INT_MAX );
\add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\Theme::wp_enqueue_scripts' );
\add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\Overrides::dequeue' );
\add_action( 'wp_head', __NAMESPACE__ . '\\Theme::wp_head' );

/* Filters */
\add_filter( 'intermediate_image_sizes_advanced', __NAMESPACE__ . '\\Theme::remove_default_image_sizes' );
\add_filter( 'wp_calculate_image_srcset', '__return_false' ); // Disable responsive images srcset, because we use Timber for this.
\add_filter( 'jpeg_quality', __NAMESPACE__ . '\\Theme::jpeg_quality' );
\add_filter( 'the_content', __NAMESPACE__ . '\\Overrides::shortcodes' );
\add_filter( 'timber/context', __NAMESPACE__ . '\\Content::custom_header' );
\add_filter( 'timber/context', __NAMESPACE__ . '\\Content::footer_widgets' );
\add_filter( 'timber/context', __NAMESPACE__ . '\\Content::twig_menus' );
\add_filter( 'timber/context', __NAMESPACE__ . '\\Theme::settings' );
\add_filter( 'timber/post/content/show_password_form_for_protected', '__return_true' );
