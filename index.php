<?php
/**
 * Front controller
 *
 * @package WordPress
 * @subpackage WebVideoBlog
 */

$context = Timber::get_context();

if ( is_single() ) {
	$context['post'] = Timber::get_post();
	$view            = 'single.twig';
} elseif ( is_singular() ) {
	$context['post'] = Timber::get_post();
	$view            = 'singular.twig';
} elseif ( is_404() ) {
	$context['content'] = '<h1>Page not found</h1><p>Go to the <a href="' . esc_url( home_url( '/' ) ) . '">homepage</a> for more information.';
	$view               = 'page-plugin.twig';
} elseif ( is_search() ) {
	$context['posts']        = array();
	$context['pagination']   = false;
	$context['search_query'] = '';
	if ( isset( $_GET['s'] ) ) {
		$context['search_query'] = filter_var( wp_unslash( (string) $_GET['s'] ), FILTER_SANITIZE_SPECIAL_CHARS );
	}
	if ( strlen( $context['search_query'] ) ) {
		$context['posts']      = Timber::get_posts();
		$context['pagination'] = Timber::get_pagination();
	}
	$context['post']['title'] = 'Search';
	$context['search_form']   = true;
	$view                     = 'search.twig';
} else {
	if ( is_archive() ) {
		$context['post']['title'] = 'Archive';
	}

	$context['head_js']    = wee\WebVideoBlog\Theme::tiles_js();
	$context['posts']      = Timber::get_posts();
	$context['pagination'] = Timber::get_pagination();
	$view                  = 'index.twig';
}

Timber::render( $view, $context );
